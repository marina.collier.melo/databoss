package DataBoss;

import java.util.Scanner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DataBoss {
	public static void main(String[] args) throws ParseException {
		System.out.printf("Informe uma Data:\n");
		Scanner ler = new Scanner(System.in);
		String data = ler.nextLine();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); 
		Date dataFormatada = formato.parse(data); 
		Calendar x = Calendar.getInstance();
		x.setTime(dataFormatada);
		x.add(Calendar.DATE, +2);
		System.out.println("Sua data +2 �: " + x.getTime());
		if((dataFormatada.getYear() % 4 == 0 && dataFormatada.getYear() % 100 != 0) || (dataFormatada.getYear() % 400 == 0)) {
			System.out.println("� bissexto!");
		}else {
			System.out.println("N�o � bissexto!");
		}
	}
}
